﻿namespace Sourcegraph.Demo;

/// <summary>
///     商品。
/// </summary>
public class Product
{
    /// <summary>
    ///     商品のタイトル。
    /// </summary>
    public string Title { get; init; }

    /// <summary>
    ///     商品の価格。
    /// </summary>
    public decimal Price { get; init; }

    public override string ToString()
    {
        return $"{Title}";
    }
}