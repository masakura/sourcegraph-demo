﻿using System.Text;

namespace Sourcegraph.Demo;

/// <summary>
///     ショッピングカート。
/// </summary>
public sealed class Cart
{
    private readonly List<Product> _products = new();

    /// <summary>
    ///     ショッピングカートに商品を追加。
    /// </summary>
    /// <param name="product">追加する商品。</param>
    public void Add(Product product)
    {
        _products.Add(product);
    }

    public override string ToString()
    {
        var builder = new StringBuilder();
        decimal total = 0;

        foreach (var product in _products)
        {
            builder.AppendLine($"{product.Title}\t{product.Price}");
            total += product.Price;
        }

        builder.AppendLine();
        builder.AppendLine($"{total}");

        return builder.ToString();
    }
}