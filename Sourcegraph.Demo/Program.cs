﻿// See https://aka.ms/new-console-template for more information

using Sourcegraph.Demo;

var cart = new Cart();
cart.Add(new Product {Title = "GitLab実践ガイド", Price = 2750});
cart.Add(new Product {Title = "Docker実践ガイド 第2版", Price = 4180});

Console.WriteLine(cart);